#include "crypkt/crypkt.hpp"

#include <cstdio>
#include <string>

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include "plog/Log.h"

#include "base64.hpp"


// For Debugging 
#include <bitset>

void logPublicKey(unsigned char* publickey){

    char temp[2*crypto_box_PUBLICKEYBYTES + 1];
    sodium_bin2hex(temp, 2*crypto_box_PUBLICKEYBYTES + 1,
                    publickey, crypto_box_PUBLICKEYBYTES);
    PLOGD << "My public key: " << temp;
}

void logNonce(unsigned char* nonce){

    char temp[2*crypto_box_NONCEBYTES + 1];
    sodium_bin2hex(temp, 2*crypto_box_NONCEBYTES + 1,
                    nonce, crypto_box_NONCEBYTES);
    PLOGD << "Nonce value: " << temp;
}

void logData(std::string s) {
    PLOGD << "leng " << s.length();
    for (size_t i = 0; i < s.length(); i++)
        std::cout << std::bitset<8>(s.c_str()[i]);
}
// End Debugging 


// Encrypt and decrypt data to be stored in profile structure

ustring encrypt_data(const ustring dataToEncrypt, const std::string password) {


    //Generating key from supplied password
    unsigned char salt[SALT_LEN];
    unsigned char generatedCryptoKey[KEY_LEN];

    //TODO: figure out a better way to generate and store salt instead of all zeros
    PLOGD << "Generating salt";
    sodium_memzero(salt, sizeof(salt));

    if (crypto_pwhash(generatedCryptoKey, sizeof generatedCryptoKey, password.c_str(), password.size(), salt,
                      crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_INTERACTIVE,
                      crypto_pwhash_ALG_DEFAULT) != 0) {
        PLOGE << "Error when running Argon2, out of memory";
        return ustring();
    } else {
        PLOGD << "Key generated successfully";
    }


    ustring buffer;

    // unsigned char bufferIn[FILE_CHUNK_SIZE];
    unsigned char bufferOut[FILE_CHUNK_SIZE + crypto_secretstream_xchacha20poly1305_ABYTES];
    unsigned char header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];
    crypto_secretstream_xchacha20poly1305_state st;


    unsigned long long outputLen;
    unsigned char tag;

    ustring chunk;
    crypto_secretstream_xchacha20poly1305_init_push(&st, header, generatedCryptoKey);

    chunk.assign(header, crypto_secretstream_xchacha20poly1305_HEADERBYTES);
    buffer += chunk;

    tag = 0;
    for (int i=0; tag != crypto_secretstream_xchacha20poly1305_TAG_FINAL; i++) {

        try {
            chunk = dataToEncrypt.substr(i *  FILE_CHUNK_SIZE, FILE_CHUNK_SIZE);
        } catch (const std::out_of_range& e) {
            // never happens
            break;
        }
        tag = (chunk.size() < FILE_CHUNK_SIZE) ? crypto_secretstream_xchacha20poly1305_TAG_FINAL: 0;
        crypto_secretstream_xchacha20poly1305_push(&st, bufferOut, &outputLen, chunk.c_str(), chunk.size(),
                                                   NULL, 0, tag);
        // fwrite(bufferOut, 1, (size_t) outputLen, openOutputFile);
        chunk.assign(bufferOut, outputLen);
        buffer += chunk;
    }
    PLOGD << "encryptStream size : " << buffer.size();


    //Zero out key in memory
    PLOGD << "Zeroing crypto key in memory using sodium_memzero() function";
    sodium_memzero(generatedCryptoKey, sizeof(generatedCryptoKey));
    PLOGI << "File encryption finished";

    return buffer;

}

ustring decrypt_data(const ustring dataToDecrypt, const std::string password) {

    //Generating key from supplied password
    unsigned char salt[SALT_LEN];
    unsigned char generatedCryptoKey[KEY_LEN];

    //TODO: figure out a better way to generate and store salt instead of all zeros
    PLOGD << "Generating salt";
    sodium_memzero(salt, sizeof(salt));

    if (crypto_pwhash(generatedCryptoKey, sizeof generatedCryptoKey, password.c_str(), password.size(), salt,
                      crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_INTERACTIVE,
                      crypto_pwhash_ALG_DEFAULT) != 0) {
        PLOGE << "Error when running Argon2, out of memory";
        return ustring();
    } else {
        PLOGD << "Key generated successfully";
    }


    //unsigned char bufferIn[FILE_CHUNK_SIZE + crypto_secretstream_xchacha20poly1305_ABYTES];
    unsigned char bufferOut[FILE_CHUNK_SIZE];
    // unsigned char header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];
    crypto_secretstream_xchacha20poly1305_state st;


    unsigned long long outputLen;
    size_t readLen = FILE_CHUNK_SIZE + crypto_secretstream_xchacha20poly1305_ABYTES;
    unsigned char tag;


    ustring chunk;
    
    PLOGD << "dataToDecrypt size: " << dataToDecrypt.size();
    chunk = dataToDecrypt.substr(0, crypto_secretstream_xchacha20poly1305_HEADERBYTES);
    
    if (chunk.size() < crypto_secretstream_xchacha20poly1305_HEADERBYTES) {
        PLOGE << "Incomplete header" << chunk.size() << " < " << crypto_secretstream_xchacha20poly1305_HEADERBYTES;
        sodium_memzero(generatedCryptoKey, sizeof(generatedCryptoKey));
        return ustring();
    }

    // collect header
    if (crypto_secretstream_xchacha20poly1305_init_pull(&st, chunk.c_str(), generatedCryptoKey) != 0) {
        PLOGE << "Malformed header";
        sodium_memzero(generatedCryptoKey, sizeof(generatedCryptoKey));
        return ustring();
    }

    ustring buffer;
    tag = 0;
    bool done = false;

    for (int i=0; !done ; i++) {
        
        try {
            chunk = dataToDecrypt.substr(crypto_secretstream_xchacha20poly1305_HEADERBYTES + i *  readLen, readLen);
        } catch (const std::out_of_range& e) {
            // never happens
            break;
        }
        done = (chunk.size()  < readLen);
        PLOGD << "New chunk! Last? " << done;

        if (crypto_secretstream_xchacha20poly1305_pull(&st, bufferOut, &outputLen, &tag,
                                                       chunk.c_str(), chunk.size(), NULL, 0) != 0) {
            PLOGD << "Corrupted chunk encountered";
            sodium_memzero(generatedCryptoKey, sizeof(generatedCryptoKey));
            return ustring();
        }
        if (tag == crypto_secretstream_xchacha20poly1305_TAG_FINAL && !done) {
            PLOGD << "Premature end (end of file to decrypt reached before the end of the stream)";
            sodium_memzero(generatedCryptoKey, sizeof(generatedCryptoKey));
            return ustring();
        }
        chunk.assign(bufferOut, outputLen);
        buffer += chunk;
    }

    //Zero out key in memory
    PLOGD << "Zeroing crypto key in memory using sodium_memzero() function";
    sodium_memzero(generatedCryptoKey, sizeof(generatedCryptoKey));
    PLOGI << "File decryption finished";

    return buffer;

}

// Conversion from to u32, system indipendent


std::string utos(ustring u){
     return std::string(u.begin(), u.end());
}
ustring stou(std::string s){
     return ustring(s.begin(), s.end());
}

uint32_t ctou32 (const unsigned char* buffer) {
    uint32_t num = (uint32_t)buffer[0] << 24 |
                    (uint32_t)buffer[1] << 16 |
                    (uint32_t)buffer[2] << 8  |
                    (uint32_t)buffer[3];
    return num;
}

void u32toc (const uint32_t num, unsigned char* buffer) {

    buffer[0] = (num >> 24) & 0xff;  /* high-order (leftmost) byte: bits 24-31 */
    buffer[1] = (num >> 16) & 0xff;  /* next byte, counting from left: bits 16-23 */
    buffer[2] = (num >>  8) & 0xff;  /* next byte, bits 8-15 */
    buffer[3] = num         & 0xff;  /* low-order byte: bits 0-7 */

}


/*
 * CryptoProtocol class
 * This class takes care of converting long texts into small encrypted segments,
 * exchange keys and nonce on first communication.
 * It also received message chunks and decrypts them. No attempt is made
 * to reassamble messages though.
 */


int CryptoProtocol::checkStatus() {
    // Checks current setup

    // Utterly broken state
    if (sodium_is_zero(me.secretkey, crypto_box_SECRETKEYBYTES)) return -1;

    // Used to check if plaintext messages should be sent in the initial
    // setup phase
    if (sodium_is_zero(bob.publickey, crypto_box_PUBLICKEYBYTES) ||
        sodium_is_zero(bob.nonce, crypto_box_NONCEBYTES)) return 0;

    // keys and nonce have been exchanged
    return 2;
}

Packet CryptoProtocol::messageToPacket(const ustring &msg) {
    // deserliaze unsigned char data into a Packet structure.
    // All possible sanity checks are attempt to validate the result
    // since cereal may silently fail.

    std::stringstream ss;
    ss << utos(msg);

    Packet p;
    {
        cereal::PortableBinaryInputArchive iarchive(ss);
        iarchive(p);
    }

    if ((p.type != packetType::KEYS) && (p.type != packetType::NONCE) && (p.type != packetType::MESSAGE))
    {
        p.type = INVALID;
        p.size = 0;
    }

    if ((p.type == packetType::KEYS) && (p.size != crypto_box_PUBLICKEYBYTES))
    {
        p.type = INVALID;
        p.size = 0;
    }

    if ((p.type == packetType::NONCE) && (p.size != crypto_box_NONCEBYTES))
    {
        p.type = INVALID;
        p.size = 0;
    }

    return p;
}

ustring CryptoProtocol::packetToMessage(const Packet &p) {
    // serialize a packet into a unsigned int packet

    std::stringstream ss;
    {
        cereal::PortableBinaryOutputArchive oarchive(ss);
        oarchive(p);
    }

    // check lenght
    assert( ss.tellp() == PACKET_SIZE );
    
    return stou(ss.str());
}

int CryptoProtocol::initialize() {
    // Init cryptography library
    int r = sodium_init();

    // move this to static method
    sodium_memzero(me.publickey, crypto_box_PUBLICKEYBYTES);
    sodium_memzero(me.secretkey, crypto_box_SECRETKEYBYTES);
    randombytes_buf(me.nonce, crypto_box_NONCEBYTES);
    me.nonce_counter = 0;

    sodium_memzero(bob.publickey, crypto_box_PUBLICKEYBYTES);
    sodium_memzero(bob.secretkey, crypto_box_SECRETKEYBYTES);
    sodium_memzero(bob.nonce, crypto_box_NONCEBYTES);
    bob.nonce_counter = 0;

    return r;

}

bool CryptoProtocol::generateKeyPair() {
    return crypto_box_keypair(me.publickey, me.secretkey) == 0;
}

bool CryptoProtocol::loadMe(std::string meData, std::string pass) {
    return loadInfo(meData, me, pass);
}
bool CryptoProtocol::loadBob(std::string bobData, std::string pass) {
    return loadInfo(bobData, bob, pass);
}

bool CryptoProtocol::saveMe(std::string& meData, std::string name, std::string pass) {
    return saveInfo(meData, me, name, pass);
}
bool CryptoProtocol::saveBob(std::string& bobData, std::string name, std::string pass) {
    return saveInfo(bobData, bob, name, pass);
}

bool CryptoProtocol::loadInfo(const std::string fileContent, CryptoIdentity &who, const std::string password) {
    
    // Load info
    std::stringstream is;
    is << fileContent;

    StoreIdentity myIdentity;
    {
        cereal::JSONInputArchive iarchive(is); // Create an input archive
        iarchive( CEREAL_NVP( myIdentity ) ); // Read the data from the archive
    }

    std::stringstream ss;
    if (myIdentity.pwEncrypted) {
        // decrypt with pw
        ustring res = decrypt_data ( stou( base64::from_base64(myIdentity.cryptoInfo) ) , password);
        if (res.empty()) return false;
        ss << utos ( res );
    } else {
        ss << base64::from_base64(myIdentity.cryptoInfo);
    }

    {
        cereal::BinaryInputArchive iarchive(ss);
        iarchive(who);
    }
    return true;
}

bool CryptoProtocol::saveInfo(std::string& fileContent, const CryptoIdentity who, std::string name, const std::string password) {

    // Save info
    StoreIdentity myIdentity;
    myIdentity.name = name;
    myIdentity.pwEncrypted = !password.empty();

    std::stringstream ss;
    {
        cereal::BinaryOutputArchive oarchive(ss);
        oarchive(who);
    }

    if (myIdentity.pwEncrypted) {
        // encrypt with pw 
        ustring enc = encrypt_data(stou(ss.str()), password);
        if (enc.size() == 0) return false;
        myIdentity.cryptoInfo = base64::to_base64(utos(enc));
    } else {
        myIdentity.cryptoInfo = base64::to_base64(ss.str());
    }


    std::stringstream file;
    {
        cereal::JSONOutputArchive oarchive( file );
        oarchive( CEREAL_NVP( myIdentity ) );
    }

    fileContent = file.str();
    return true;
}

bool CryptoProtocol::checkPacketSize(){
    Packet p;
    std::stringstream ss;
    {
        cereal::PortableBinaryOutputArchive oarchive(ss); // Create an output archive
        oarchive(p); // Write the data to the archive
    }

    return (ss.str().size() + crypto_box_MACBYTES + NONCE_COUNTER_SIZE == PAYLOAD);
}

void CryptoProtocol::prepareMessage(const commands cmd, const unsigned char * data, const size_t len, packetGenerationErrors &err) {

    if (me.nonce_counter == UINT32_MAX) {
        PLOGD << "No messages left!";
        err = PGE_NO_MSGS_LEFT;
        return;
    } else if ((me.nonce_counter >= 2) && (checkStatus() == 0)){
        PLOGD << "Wont communicate without keys anymore";
        err = PGE_KEYS_NEEDED;
        return;
    }

    // add both keys and nonce to sending queue
    if (cmd == CMD_KEYS) {
        {
            Packet p;
            p.type = packetType::KEYS;
            p.size = (uint8_t) crypto_box_PUBLICKEYBYTES;
            std::memcpy(p.data, me.publickey, crypto_box_PUBLICKEYBYTES);
            if (enqueueOutboundPacket(p) != PEE_NONE ) {
                err = PGE_QUEUE_FAILED;
                return;
            }
        }
        {
            Packet p;
            p.type = packetType::NONCE;
            p.size = (uint8_t) crypto_box_NONCEBYTES;
            std::memcpy(p.data, me.nonce, crypto_box_NONCEBYTES);
            if (enqueueOutboundPacket(p) != PEE_NONE ) {
                err = PGE_QUEUE_FAILED;
                return;
            }
        }

    } else if (cmd == CMD_NONCE) {
    
        unsigned char new_nonce[crypto_box_NONCEBYTES] = { 0 };
        randombytes_buf(new_nonce, crypto_box_NONCEBYTES);
        
        Packet p;
        p.type = packetType::NONCE;
        p.size = (uint8_t) (sizeof me.nonce);
        std::memcpy(p.data, new_nonce, sizeof me.nonce);
    
        if (enqueueOutboundPacket(p) != PEE_NONE ) {
            err = PGE_QUEUE_FAILED;
            return;
        }
    
        std::memcpy(me.nonce, new_nonce, sizeof me.nonce);

    } else if (cmd == CMD_MESSAGE) {
        size_t sent_len = 0;

        while (true) {

            Packet p;
            p.type = packetType::MESSAGE;
            p.size = (uint8_t) std::min((size_t) MESSAGE_LEN_E, len - sent_len);
            std::memcpy(p.data, &(data[sent_len]), p.size);
            
            if (enqueueOutboundPacket(p) != PEE_NONE) {
                err = PGE_QUEUE_FAILED;
                return;
            }
            
            sent_len += p.size;
            if (sent_len >= len) break;
        }
        
    } else {
        // ?? unknown command
        err = PGE_INVALID_COMMAND;
        return;
    }
    err = PGE_NONE;
}

packetEncryptionErrors CryptoProtocol::enqueueOutboundPacket(const Packet p) {

    PLOGD << "Encrypting message"; // with size :" << ((int) p.size);

    ustring pData = packetToMessage(p) ; // stou(ss.str());
    ustring message;

    switch (me.nonce_counter){
        case 0: // this is the first message that we send
        case 1: // this is the second message that we send
        {
            // try plain text
            if (p.type == packetType::KEYS || p.type == packetType::NONCE) {
                message = pData;
            } else {
                // error
                PLOGE << "Trying to send plaintex data. I'll never allow that!";
                return PEE_PLAIN_FORBIDDEN;
            }
            break;
        }

        default: // we have both keys and nonce, does he have ours?
        {
            unsigned char nonce[crypto_box_NONCEBYTES];
            sodium_memzero(nonce, crypto_box_NONCEBYTES);
            sodium_add(nonce, me.nonce, crypto_box_NONCEBYTES);
            sodium_add(nonce, (const unsigned char*) &me.nonce_counter, NONCE_COUNTER_SIZE);

            unsigned char header[NONCE_COUNTER_SIZE];
            unsigned char ciphertext[PAYLOAD - NONCE_COUNTER_SIZE];

            PLOGD << "Adding nonce counter";
            std::memcpy(header, (const unsigned char*) &me.nonce_counter, NONCE_COUNTER_SIZE);
            message.assign(header, NONCE_COUNTER_SIZE);

            if (crypto_box_easy(ciphertext, pData.c_str(), pData.size(), nonce, bob.publickey, me.secretkey) != 0) {
                /* error */
                PLOGE << "I could not encrypt";
                return PEE_ENC_FAILED;;
            }
            PLOGD << "Encrypted data";
            message.append(ciphertext, PAYLOAD - NONCE_COUNTER_SIZE);

            break;
        }
    }
    me.nonce_counter++;
    outPacketsE.push_back(message);
    return PEE_NONE;
}

size_t CryptoProtocol::popPacket( char * data ) {

    if ( outPacketsE.empty() ) return 0;
    //PLOGD << "Outbound Packet queue lenght: " << outPacketsE.size();

    ustring p = outPacketsE.at(0);
    //PLOGD << "Outbound Packet lenght: " << p.size();

    // safety check
    assert(p.length() <= PAYLOAD);

    std::memcpy(data, utos(p).c_str(), p.length());

    outPacketsE.erase(outPacketsE.begin());

    return p.length();
}


packetDecryptionErrors CryptoProtocol::receiveQueue(unsigned char* data, size_t dataLength) {
    PLOGD << "Received one packet " << dataLength;

    // fix types
    ustring message;

    // try with nonce
    if (checkStatus() == 2){ // I have bob's keys and nonce, I will only allow this decryption type
        if (dataLength != PAYLOAD) {
            PLOGE << "Invalid packet size";
            return PDE_INVALID_SIZE;
        }

        /* Decrypt */    
        unsigned char decrypted[PAYLOAD - crypto_box_MACBYTES];
        unsigned char nonce[crypto_box_NONCEBYTES] = { 0 };

        // prepare nonce
        sodium_add(nonce, bob.nonce, crypto_box_NONCEBYTES);


        // read nonce from packet
        bob.nonce_counter = ctou32(data);
        // TODO: check that has increased and or not reused??

        sodium_add(nonce, (const unsigned char*) data, sizeof(uint32_t));
        // or shoud we use ctou32??

        // decrypt message with provided nonce
        PLOGD << "Decrypt data";
        message.assign(data + NONCE_COUNTER_SIZE, dataLength - NONCE_COUNTER_SIZE); //logData(utos(message));

        if (crypto_box_open_easy(decrypted, message.c_str(), message.size(), nonce,
                            bob.publickey, me.secretkey) == 0) {
        
            message.assign(decrypted, message.size() - crypto_box_MACBYTES);
        
            inPacketsE.push_back(message);
        } else {
            PLOGD << "Failed to decrypt ";
            return PDE_DEC_FAILED;
        }

    } else if (checkStatus() == 0) {

        // handle failure;
        PLOGD << "I use plain text mode for keys and nonce";
        //TODO: check lenght
        message.assign(data, dataLength);
        if (messageToPacket(message).type == packetType::KEYS ||
            messageToPacket(message).type == packetType::NONCE)
        {
            inPacketsE.push_back(message);
        } else {
            //refuse!
            return PDE_PLAIN_FORBIDDEN;
        }
    } else {
        // This is very very strange and should never happen!
        PLOGE << "Unknown error!";
        return PDE_UNKNOWN_ERROR;
    }
    return PDE_NONE;
}



bool CryptoProtocol::processIncomingPackets(packetType &pt, char* message, size_t &messageLength, packetParsingErrors &err)
{

    if (inPacketsE.empty()) return false;

    auto msg = inPacketsE.at(0);

    Packet p = messageToPacket(msg);

    inPacketsE.erase(inPacketsE.begin());

    PLOGD << "type " << int(p.type);

    pt = p.type; // remove me!! remove packetType from this api
    switch (p.type)
    {
        case packetType::INVALID:
            PLOGE << "Error parsing packet";
            err = PPE_INVALID;
            messageLength = 0;
            break;

        case packetType::KEYS:
            PLOGD << "GOT keys";

            // vital checks
            assert(p.size == crypto_box_PUBLICKEYBYTES);
            assert(!sodium_is_zero(p.data, crypto_box_PUBLICKEYBYTES));

            std::memcpy(bob.publickey, p.data, crypto_box_PUBLICKEYBYTES);
            messageLength = 0;
            err = PPE_NONE;
            break;

        case packetType::NONCE:
            PLOGD << "GOT nonce";

            // vital checks
            assert(p.size == (sizeof bob.nonce));
            assert(!sodium_is_zero(p.data, crypto_box_NONCEBYTES));

            std::memcpy(bob.nonce, p.data, p.size);
            // logNonce(bob.nonce);
            bob.nonce_counter = 0;
            messageLength = 0;
            err = PPE_NONE;
            break;

        case packetType::MESSAGE:
            assert(p.size <= MESSAGE_LEN_E);
            strncpy(message, (const char *) p.data, p.size);
            messageLength = p.size;
            err = PPE_NONE;
            break;
    }

    // check again for other packages
    return true;
}
