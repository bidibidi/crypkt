#pragma once

#include <cassert>
#include <cstdio>
#include <string>

#include <istream>
#include <ostream>
#include <cmath>

#include <sodium.h>

#include "cereal/types/unordered_map.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/archives/portable_binary.hpp"
#include "cereal/archives/json.hpp"


// ggwave fixed message size
#define PAYLOAD 64

// nonce counter transmitted as header
#define NONCE_COUNTER_SIZE sizeof(uint32_t)

// size of serialization
#define BINARY_STRUCTURE_SIZE 3

// maximum size of message transmitted within a packet
#define MESSAGE_LEN_E (PAYLOAD - crypto_box_MACBYTES - BINARY_STRUCTURE_SIZE - NONCE_COUNTER_SIZE)

// size of a packet
#define PACKET_SIZE (MESSAGE_LEN_E + BINARY_STRUCTURE_SIZE)

// definitions for profile encryption
#define KEY_LEN crypto_secretstream_xchacha20poly1305_KEYBYTES
#define SALT_LEN crypto_pwhash_SALTBYTES
#define FILE_CHUNK_SIZE 4096

typedef std::basic_string<unsigned char> ustring;

enum packetType : uint8_t { INVALID, KEYS, NONCE, MESSAGE};
enum commands { CMD_KEYS, CMD_MESSAGE, CMD_NONCE};

enum packetParsingErrors {PPE_NONE, PPE_INVALID };
enum packetGenerationErrors {PGE_NONE, PGE_NO_MSGS_LEFT, PGE_QUEUE_FAILED, PGE_INVALID_COMMAND, PGE_KEYS_NEEDED };
enum packetEncryptionErrors {PEE_NONE, PEE_ENC_FAILED, PEE_PLAIN_FORBIDDEN, PEE_UNKNOWN_ERROR};
enum packetDecryptionErrors {PDE_NONE, PDE_DEC_FAILED, PDE_PLAIN_FORBIDDEN, PDE_INVALID_SIZE, PDE_UNKNOWN_ERROR};

struct Packet
{
  packetType type;
  uint8_t size;
  unsigned char data[MESSAGE_LEN_E];
  template <class Archive>
  void serialize( Archive & ar )
  {
    ar( type, size, data );
  }
};

/* Structure storing keys and nonce.
 * nonce_counter keeps track of increments used to make nonce unique
 */
struct CryptoIdentity
{
    unsigned char publickey[crypto_box_PUBLICKEYBYTES] = { 0 };
    unsigned char secretkey[crypto_box_SECRETKEYBYTES] = { 0 };
    unsigned char nonce[crypto_box_NONCEBYTES] = { 0 };
    uint32_t nonce_counter = 0;

    // serialization
    template <class Archive>
    void serialize( Archive & ar )
    {
       ar( publickey, secretkey, nonce, nonce_counter );
    }
};

/* Structure used to store a profile. The cryptographic information are
 * stored as base64 data (possibly encrypted).
 * pwEncrypted specifies wether cryptoInfo is password protected.
 * name is a plain text name of the profile
 */
struct StoreIdentity
{
    std::string name = "";
    std::string cryptoInfo = "";
    bool pwEncrypted = false;

    // serialization
    template <class Archive>
    void serialize( Archive & ar )
    {
       ar( CEREAL_NVP(name), CEREAL_NVP(cryptoInfo), CEREAL_NVP(pwEncrypted));
    }
};



/* CryptoProtocol class.
*
* CryptoProtocol implements a simple encryption protocol based on
* libsodium to encrypt messages exchanged through ggwave.
*/

class CryptoProtocol {

    private:

        // my identity, with secret and public keys and nonces
        CryptoIdentity me;
        // mate's identity
        CryptoIdentity bob;

        // vector hosting packets received through ggwave
        std::vector<ustring> inPacketsE;
        // vector hosting packets to be sent through ggwave
        std::vector<ustring> outPacketsE;

        // converts unsigned char string to packet by deserializing it
        static Packet messageToPacket(const ustring &msg);
        // serialize packet to ustring (unsigned chat string)
        static ustring packetToMessage(const Packet &p);

        bool loadInfo(const std::string fileContent, CryptoIdentity &who, const std::string pass);

        bool saveInfo(std::string& fileContent, const CryptoIdentity who, std::string name, const std::string pass);

        // add packer to send queue 
        packetEncryptionErrors enqueueOutboundPacket(const Packet p);

    public:

        // default constructor
        CryptoProtocol() = default;

        // default destructor
        ~CryptoProtocol() {};

        // initialize identities and libsodium
        int initialize();

        // check packets lenght
        bool checkPacketSize();

        // checks current setup.
        int checkStatus();

        // creates new keypair.
        bool generateKeyPair();

        // loads profile that includes secrect key from string meData using password pass (if encrypted, otherwise pass is ignored)
        bool loadMe(std::string meData, std::string pass);

        // loads profile that does not include secrect key from string bobData using password pass (if encrypted, otherwise pass is ignored)
        bool loadBob(std::string bobData, std::string pass);

        // Stores profile with secret key in meData. Profile is encrypted if pass is not empty.
        bool saveMe(std::string& meData, std::string name, std::string pass);

        // Stores profile without secret key in meData. Profile is encrypted if pass is not empty. 
        bool saveBob(std::string& bobData, std::string name, std::string pass);

        // serialize and possibly encrypts a packet and ads it to sending queue.
        void prepareMessage(const commands cmd, const unsigned char * data, const size_t len, packetGenerationErrors &err);

        // get packet from sending queue
        size_t popPacket( char * data );

        // decrypts packet from queue and returns content
        bool processIncomingPackets(packetType &p, char* message, size_t &messageLength, packetParsingErrors &err);

        // adds packet to receive queue
        packetDecryptionErrors receiveQueue (unsigned char* data, size_t dataLength);

};
