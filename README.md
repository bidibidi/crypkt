# CryPkt

A super simple library to setup authenticated asymmetric encryption using libsodium, splitting long messages into chunks and encrypting each of them. The decryption part is also implemented. It just provides and encryption layer for ggwave, where packet size is constrained.

Reading the source code makes you cry, at least for the time being.

## Usage

Init submodules and build with cmake.
